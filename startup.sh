#!/bin/bash
###########
### Provides an easy way to run my pihole-led.sh script on boot in it's own screen session.
### https://github.com/dstinebaugh/pihole-led
### Built for my script however you can modify as needed of course.
### Don't forget to change the Settings block below to match your locations!
### BLINKLOC should be the folder that has pihole-led.sh in it
### HISTORY is the number of scrollback lines in the screen session. Unless you have uncommented the echo line in the
###  pihole script, there's not really any need for a scrollback so I've lowered it to keep memory use down on the pi.
###########
### Version:            0.2
### Mantainer:          D Guerrero
### original Author:    Daniel Stinebaugh
### Attribution:        http://www.stinebaugh.info/pihole-led-startup-script/
### original Date:      04/20/2016
### License:            Copyleft. Enjoy!
###########

#### Settings
# has to be run as root in order to modify the blue led trigger
USERNAME='root'
# folder holding the pihole-led.sh file
BLINKLOC='/opt/odroid-c2-pihole-led/'
# script name to run
INVOCATION="pihole-led.sh"
# history log of screen session keeping it small to save resources.
HISTORY=10
### End Settings


ME=`whoami`

if pgrep -u $USERNAME -f $INVOCATION > /dev/null
then
        echo "pihole-led is already running!"
        echo " Check your screen sessions."
        exit 1
else
        echo "Starting pihole-led.sh in a new screen session..."
        cd $BLINKLOC
        if [ $ME == $USERNAME ] ; then
                whoami
                bash -c "screen -h $HISTORY -dmS blink /bin/bash $BLINKLOC$INVOCATION"
        else
                su - $USERNAME -c "screen -h $HISTORY -dmS blink /bin/bash $BLINKLOC$INVOCATION"
        fi
        sleep 5
        if pgrep -u $USERNAME -f $INVOCATION > /dev/null
        then
                logger -s "pihole-led.sh is now running."
        else
                logger -s "[ALERT] Couldn't Validate pihole-led.sh Started Properly!"
        fi
fi
echo "Startup Script complete."

exit 0

