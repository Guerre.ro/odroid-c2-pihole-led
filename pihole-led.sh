#!/bin/bash
###########
### This pihole-led script is built for the odroid C2
### Built for pihole log's however you can modify as needed of course.
###########
### Version:            0.2
### Mantainer:          D Guerrero
### original Author:    Daniel Stinebaugh
### Attribution:        http://www.stinebaugh.info/pihole-led-startup-script/
### original Date:      04/20/2016
### License:            Copyleft. Enjoy!
###########

# Blink for 3 seconds to visualy show the script is starting.
echo default-on >> /sys/class/leds/c2\:blue\:alive/trigger
sleep 3
echo none >> /sys/class/leds/c2\:blue\:alive/trigger
tail -f /var/log/pihole.log | while read INPUT
do
    #updated to use log syntax of Pi-hole v5.2.2 
    if [[ "$INPUT" == *"blocked "* ]]; then
        echo default-on >> /sys/class/leds/c2\:blue\:alive/trigger
        sleep 0.2
        echo none >> /sys/class/leds/c2\:blue\:alive/trigger
        echo "$(date) another ad bites the dust!"
        sleep 0.1
    fi
done
