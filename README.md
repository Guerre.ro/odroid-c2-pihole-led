# pihole-led (Odroid C2 Fork)
## Realtime LED alerts when an ad is blocked when using pi-hole.

This fork is intended to be used on an Odroid C2 using Armbian. o you ise a different board/OS please adjust the values accordingly.

pihole-led.sh will parse /var/log/pihole.log for the string "blocked" and if found it will set the Blue heartbeat led on briefly (200ms) then turn it back off making it blink once for each entry found in near realtime.

Comment out the echo line 25 in the loop if you don't want textual updates to the terminal.

it is also included a startup script to launch the pihole-led.sh script on boot. Just edit the values in the Setting block and include it's location to your /etc/rc.local or cron file.

The startup script will launch a new screen session named "blink" so the screen package is also required to be installed. (It's in your repos most likely) 

Attribution and more info: http://www.stinebaugh.info/get-led-alerts-for-each-blocked-ad-using-pi-hole/
